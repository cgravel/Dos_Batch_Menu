#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#AutoIt3Wrapper_Res_File_Add=open.bmp, rt_bitmap, OPEN_BMP
#AutoIt3Wrapper_Res_File_Add=save.bmp, rt_bitmap, SAVE_BMP
#AutoIt3Wrapper_Res_File_Add=delete.bmp, rt_bitmap, DELETE_BMP
#AutoIt3Wrapper_Res_File_Add=edit.bmp, rt_bitmap, EDIT_BMP
#AutoIt3Wrapper_Res_File_Add=additem.bmp, rt_bitmap, ADDITEM_BMP
;#AutoIt3Wrapper_Res_Icon_Add=C:\Program Files\AutoIt3\Icons\au3.ico

;~ #EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <File.au3>
#include <Array.au3>
#include <GUIConstants.au3>
#include <GuiConstantsEx.au3>
#include <GUIListBox.au3>
#include <ButtonConstants.au3>
#include <ListviewConstants.au3>
#include <SliderConstants.au3>
#include <StaticConstants.au3>
#include <GuiListView.au3>
#include <EditConstants.au3>
#include <GuiButton.au3>
#include <Misc.au3>
#include <string.au3>

#include "resources.au3" ;// included for using resource files
;#include <__Helper.au3>
;#include <WindowsConstants.au3> ; 1 constant

;#include <StaticConstants.au3>
;~ #include <GuiConstantsEx.au3>


#include <Array.au3>

;~ #include <GuiImageList.au3>
;~ #include <GuiListView.au3>
;~ #include <GuiImageList.au3>

#include <GuiConstantsEx.au3>
;~ #include <GuiListView.au3>
#include <GuiImageList.au3>


;######## change format to make it look like an excel sheet
;	Each item will contain:
; Color Indicating Errors, Number, Name, Path, Exe, RunMe.Bat (Yes or No)
; Outside is a box to change base address

_Singleton("Dos Batch Menu", 0)

;#include <GUIOnChangeRegister.au3>

#region Presets
;Opt("pixelcoordmode", 2)
Opt("CaretCoordMode", 0)
Opt("mousecoordmode", 0)
Opt("GUIOnEventMode", 1) ; set to 0 to enable guigetmsg
;Opt("GUICoordMode", 1)
;Opt("WinTitleMatchMode", 4)
;Opt("MouseClickDelay", 20)
;Opt("MouseClickDownDelay", 30)
;Opt("MouseClickDragDelay", 250)

Global $title_c = "Dos Batch Menu Helper by Chris"
;~ Global $guimaxY = 350
;~ Global $guimaxX = 200
Global $guimaxX = 600
Global $guimaxY = 492
;~ Global $hGUI = GUICreate($title_c, $guimaxX, $guimaxY, @DesktopWidth - ($guimaxX + 8), 5)
;~ Global $hGUI = GUICreate("Current Batch Menu Configuration", $guimaxX, $guimaxY, 2, 2, "", "", $hGUI)
Global $hGUI = GUICreate($title_c, $guimaxX, $guimaxY, 5, 5)
;~ Global $hGUI = GUICreate("Current Batch Menu Configuration", $guimaxX, $guimaxY, 2, 2, "", "", $hGUI)

Global $scriptRun = ""
Global $temp = 0
;~ Global $dir = "C:\Games\Dos Games\"
Global $dir = 0
Global $countMenuItems
Global $countMenus
Global $strRunBat ; This will contain the batch script for the run.bat
Global $hwndListViewBatch
Global $fDblClk = False

Global $lvHotItem
Global $lvHotItemSub
Global $oldHotItem = -1
Global $oldHotItemSub = -1

Dim $hwndMenu[1] ; These are the handles for the open files that are being read. Note that important to declare here I think. Only temporarily open.
Dim $arStrMenus[1] ; This will contain the batch script for the menu bats
Dim $arStrMenuItems[1] ;This will contain the names of the items, will be used in the menu bats
Dim $arStrPaths[1] ; This will contain the paths to the executables, will be used in run.bat
Dim $arStrDosPaths[1] ; This will contain the Dos version of the paths
Dim $arStrExes[1] ; This will contain the exes to be run, will be used in run.bat
Dim $arStrLabel[1] ; This will contain the labels to be used in run.bat
Dim $arStrRunMeBatYN[1] ; This will contain the string Yes or No depending on the existence of the RunMeBat


;Local $dir = FileSelectFolder("Select Directory of Dos Games", "", 1)	; Select the path of the Games Folder... Returns a path
;Local $dir = "C:\Games\Dos Games" ; Switch to enable fast selecting of directory

;FileSelectFolder
;FileOpenDialog("Select the Folders to include in your Dos Batch Menu","C:/","All (*.*)",6)

;~ Dim $folder = _FileListToArray($dir, "*", 2) ; Returns list of all the folders in the directory
;~ $menusToMake = Round(($folder[0] / 40) + .49, 0) ; decide how many menus to make based on the maximum number able to display in one menu
;~ Dim $scriptMenus[$menusToMake]
;~ Dim $folderDosPath[$folder[0] + 1] ; I added one cause I want to make the elements line up, ex. $folder[2] is holding the same info as $folderDos[2] and etc.
;~ Dim $folderDos[$folder[0] + 1] ; This is the Dos name of the Folders
;~ Global $exepath[$folder[0]] ; $folder[0] contains the amount of folders in the drive and also how big the array is
;~ Global $scriptexepath[$folder[0]] ; this variable will contain the strings necessary to write the script for the batch file
;~ Global $numTotal = (UBound($folder) - 1)
#endregion Presets



$editBatchBtn = GUICtrlCreateButton("Edit Previous Batch Menu", 2, $guimaxY - 32, 135, 30)
GUICtrlSetOnEvent(-1, "editPreviousBatch")
$makeNewBatchBtn = GUICtrlCreateButton("Make New Batch Menu", 139, $guimaxY - 32, 135, 30)
GUICtrlSetOnEvent(-1, "makeNew")
GUICtrlSetTip(-1, "This will automatically select exes according to a previously choosen Executable." & @CRLF & _
		"This is not an AI way of picking an exe; it simple looks for RunMe.Bat's." & @CRLF & _
		"It is a good idea to make things easier in the future.", "Automatically Choose Executables")
$makeRunMeBatsBtn = GUICtrlCreateButton("MakeRunMeBats", 276, $guimaxY - 32, 135, 30)
GUICtrlSetOnEvent(-1, "MakeRunMeBats")

GUISetIcon(@SystemDir & "\mspaint.exe", 0)
GUISetOnEvent($GUI_EVENT_CLOSE, "Quit")
; Edit, Load, Save, New, Information to be editable before an ultimate create button


$hListView = GUICtrlCreateListView("#|Name|Path|Exe|RunMe.Bat| ", 2, 58, 550, 400)
;~ _GUICtrlListView_SetExtendedListViewStyle($hListView, BitOR($LVS_EX_FULLROWSELECT, $LVS_EX_SUBITEMIMAGES,$LVS_AUTOARRANGE, $LVS_ICON, $LVS_EX_SIMPLESELECT))
_GUICtrlListView_SetExtendedListViewStyle($hListView, BitOR($LVS_EX_FULLROWSELECT, $LVS_EX_SUBITEMIMAGES))
;~ _GUICtrlListView_SetExtendedListViewStyle($hListView, BitOR($LVS_AUTOARRANGE, $LVS_ICON, $LVS_EX_SIMPLESELECT))
;~ _GUICtrlListView_SetExtendedListViewStyle($hListView, $LVS_AUTOARRANGE)

;~ $hListView = _GUICtrlListView_Create($hGUI,"#|Name|Path|Exe|RunMe.Bat", 2, 58, 550, 400)
;~ _GUICtrlListView_SetExtendedListViewStyle($hListView, BitOR($LVS_EX_GRIDLINES, $LVS_EX_FULLROWSELECT, $LVS_EX_SUBITEMIMAGES, $LVS_EX_ONECLICKACTIVATE))

;One for path, One for name.... Make it look like the actual menus... That's 40 numbers... Easy, just split into two list bars.
;Include a slider at the bottom for selecting the menus
;Have it organize in alphabetical order automatically
;Have a button for inserting a game (will automatically organize)

;$GUImenuList = GUICtrlCreateLabel("Menu ", 180, 5, 50, 20, $SS_CENTER)
;$hListView = GUICtrlCreateListView("#|Name", 2, 20, 200, 385, 0x4005)
;~ _GUICtrlListView_SetColumnWidth($hListView, 0, 28)
_GUICtrlListView_SetColumnWidth($hListView, 0, 38)
_GUICtrlListView_SetColumnWidth($hListView, 1, 165)
_GUICtrlListView_SetColumnWidth($hListView, 2, 83)
_GUICtrlListView_SetColumnWidth($hListView, 3, 120)
_GUICtrlListView_SetColumnWidth($hListView, 4, 70)
;~ _GUICtrlListView_SetColumnWidth($hListView, 0, 1)
;~ _GUICtrlListView_SetColumnWidth($hListView, 1, 28)
;~ _GUICtrlListView_SetColumnWidth($hListView, 2, 165)
;~ _GUICtrlListView_SetColumnWidth($hListView, 3, 83)
;~ _GUICtrlListView_SetColumnWidth($hListView, 4, 120)
;~ _GUICtrlListView_SetColumnWidth($hListView, 5, 70)


Global $hImage = _GUIImageList_Create()
_GUIImageList_Add($hImage, _GUICtrlListView_CreateSolidBitMap(GUICtrlGetHandle($hListView), 0xFF0000, 16, 16))
_GUIImageList_Add($hImage, _GUICtrlListView_CreateSolidBitMap(GUICtrlGetHandle($hListView), 0xFFFF00, 16, 16))
_GUIImageList_Add($hImage, _GUICtrlListView_CreateSolidBitMap(GUICtrlGetHandle($hListView), 0x00FF00, 16, 16))
;~ _GUIImageList_Add($hImage, _GUICtrlListView_CreateSolidBitMap(GUICtrlGetHandle($hListView), 0xFF0000, 1, 1))
;~ _GUIImageList_Add($hImage, _GUICtrlListView_CreateSolidBitMap(GUICtrlGetHandle($hListView), 0xFFFF00, 1, 1))
;~ _GUIImageList_Add($hImage, _GUICtrlListView_CreateSolidBitMap(GUICtrlGetHandle($hListView), 0x00FF00, 1, 1))
_GUICtrlListView_SetImageList($hListView, $hImage, 1)






GUISetState()

GUICtrlCreateLabel("Base Directory:", 2, 35, 150, 20)
$GUIBasePath = GUICtrlCreateInput("", 82, 32, 250, 20, 0x800)

$GUIwarnExe = GUICtrlCreatePic("", 402, $guimaxY - 160, 25, 25)

$GUIsaveBtn = GUICtrlCreateButton("", 2, 2, 30, 30, $BS_BITMAP)
;_GUICtrlButton_SetImage($GUIsaveBtn, "C:\Programming\AutoIt3\AutoIt Programs\save.bmp")
;_GUICtrlButton_SetImage($GUIsaveBtn, "save.bmp")
_ResourceSetImageToCtrl($GUIsaveBtn, "SAVE_BMP", $RT_BITMAP)
GUICtrlSetTip(-1, "Save the changes made to the current batch file.", "Save Changes")
GUICtrlSetOnEvent(-1, "saveBatch")
;~ GUICtrlSetOnEvent(-1, "Encode")

$GUIopenBtn = GUICtrlCreateButton("", 32, 2, 30, 30, $BS_BITMAP)
;_GUICtrlButton_SetImage($GUIopenBtn, "C:\Programming\AutoIt3\AutoIt Programs\open.bmp")
;_GUICtrlButton_SetImage($GUIopenBtn, "open.bmp")
_ResourceSetImageToCtrl($GUIopenBtn, "OPEN_BMP", $RT_BITMAP)

GUICtrlSetTip(-1, "Open a Run.bat file to edit.", "Open Batch File")

$GUIdelBtn = GUICtrlCreateButton("", 552, 57, 30, 30, $BS_BITMAP)
GUICtrlSetOnEvent(-1, "delItem")
;_GUICtrlButton_SetImage($GUIdelBtn, "C:\Programming\AutoIt3\AutoIt Programs\delete.bmp")
;_GUICtrlButton_SetImage($GUIdelBtn, "delete.bmp")
_ResourceSetImageToCtrl($GUIdelBtn, "DELETE_BMP", $RT_BITMAP)
GUICtrlSetTip(-1, "Delete the current item selected in the menu", "Delete Selected Item")

$GUIaddBtn = GUICtrlCreateButton("", 552, 86, 30, 30, $BS_BITMAP)
GUICtrlSetOnEvent(-1, "addItem")
;_GUICtrlButton_SetImage($GUIaddBtn, "C:\Programming\AutoIt3\AutoIt Programs\additem.bmp")
;_GUICtrlButton_SetImage($GUIaddBtn, "additem.bmp
_ResourceSetImageToCtrl($GUIaddBtn, "ADDITEM_BMP", $RT_BITMAP)
GUICtrlSetTip(-1, "Add an item to the menu", "Add Item")

$GUIeditDir = GUICtrlCreateButton("", 332, 28, 30, 30, $BS_BITMAP)
;_GUICtrlButton_SetImage($GUIeditDir, "C:\Programming\AutoIt3\AutoIt Programs\edit.bmp")
;_GUICtrlButton_SetImage($GUIeditDir, "edit.bmp")
_ResourceSetImageToCtrl($GUIeditDir, "EDIT_BMP", $RT_BITMAP)
GUICtrlSetOnEvent(-1, "editDir")
GUICtrlSetTip(-1, "Edit the Base Directory Path", "Edit Base Directory Path")

;GUICtrlCreateGraphic(2, $guimaxY - 70, 200, 40, 0x04)


GUISetState(@SW_SHOW, $hGUI)


$lvEdit = GUICtrlCreateInput("", 0, 0, 1, 1, BitOR($ES_AUTOHSCROLL, $ES_NOHIDESEL, 0x00800000), 0)
GUICtrlSetState($lvEdit, $GUI_HIDE)
GUICtrlSetState($lvEdit, $GUI_ONTOP)
GUICtrlSetFont($lvEdit, 8.5)
#include <GuiConstantsEx.au3>
#include <GuiListView.au3>
#include <GuiImageList.au3>
#include <WindowsConstants.au3>

While 1 ; ================================================================ Main Loop ================================================================================
WEnd
Func quit()
	Exit
EndFunc   ;==>quit



Func editPreviousBatch()
	If $dir = 1 Then
		$dir = "C:\Games\Dos Games\"
	Else
;~ 		$dir = FileOpenDialog("Select the Run.bat", "C:\", "(run.bat)")
		$dir = FileOpenDialog("Select the Run.bat", "", "(run.bat)")
		$dir = StringTrimRight($dir, 7) ; ending with \
	EndIf
	ReadRunBat($dir)
	Decode()
	DisplayMenu()
EndFunc   ;==>editPreviousBatch


Func menuButtons()
	DisplayMenu()
EndFunc   ;==>menuButtons


Func DisplayMenu()
	_GUICtrlListView_DeleteAllItems($hListView)
	For $i = 0 To ($countMenuItems - 1)
		GUICtrlCreateListViewItem(($i + 1) & "|" & $arStrMenuItems[$i] & "|" & $arStrDosPaths[$i] & "|" & $arStrExes[$i] & "|" & $arStrRunMeBatYN[$i], $hListView)
		GUICtrlSetOnEvent(-1, "setItemtoEdit")
		Select
			Case (StringLen($arStrExes[$i]) > 1) And (StringInStr($arStrExes[$i], "Error") = 0) And ($arStrRunMeBatYN[$i] = "No")
				_GUICtrlListView_SetItemImage($hListView, $i, 1, 5) ;0 red 1 yellow 2 green
			Case (StringLen($arStrExes[$i]) < 2) Or (StringInStr($arStrExes[$i], "Error") = 1)
				_GUICtrlListView_SetItemImage($hListView, $i, 0, 5) ;0 red 1 yellow 2 green
			Case (StringLen($arStrExes[$i]) > 1) And (StringInStr($arStrExes[$i], "Error") = 0) And ($arStrRunMeBatYN[$i] = "Yes")
				_GUICtrlListView_SetItemImage($hListView, $i, 2, 5) ;0 red 1 yellow 2 green
		EndSelect
	Next
	;"#|Name|Path|Exe|RunMe.Bat"
EndFunc   ;==>DisplayMenu

Func getLVSelected()

EndFunc   ;==>getLVSelected

Func setItemtoEdit()
	; Save Edited Data and Hide Edit Box
	If GUICtrlGetState($lvEdit) = 80 Then; 80 is visible, 96 is hidden
		_GUICtrlListView_SetItem($hListView, GUICtrlRead($lvEdit), $oldHotItem, $oldHotItemSub) ; Replace Data
		$arStrMenuItems[$oldHotItem] = GUICtrlRead($lvEdit)
		; Include a Data Check Format...... No need to do right now because only I will use this
		Local $arLVNames[_GUICtrlListView_GetItemCount($hListView)]
		Local $insertPos = UBound($arLVNames)
		For $i = 0 To UBound($arLVNames) - 1
			$arLVNames[$i] = _GUICtrlListView_GetItemText($hListView, $i, 1)
		Next
		For $i = 0 To UBound($arLVNames) - 1
			If StringCompare($arLVNames[$i], GUICtrlRead($lvEdit)) = 1 Then
				$insertPos = $i
				ExitLoop
			EndIf
		Next
		If $insertPos > $oldHotItem Then $insertPos -= 1
		GUICtrlSetPos($lvEdit, 0, 0, 1, 1)
		GUICtrlSetState($lvEdit, $GUI_HIDE)
		Sleep(250)
		ReOrgListView()
;~ 		Local $B_DESCENDING[_GUICtrlListView_GetColumnCount($hListView)]
;~ 		_GUICtrlListView_SimpleSort($hListView, $B_DESCENDING, 1)
;~ 		sortNumbers()
		_GUICtrlListView_SetItemSelected($hListView, ($insertPos), "True", "True")
		_GUICtrlListView_EnsureVisible($hListView, $insertPos)
		ConsoleWrite(($insertPos) & @CRLF)
		ReadfromListView()
	EndIf

	;=================== Find the Selected Cell
	Local $mPos = (MouseGetPos(0) - 3)
	Local $lvItemWidth[_GUICtrlListView_GetColumnCount($hListView)]
	Local $lvItemHeight[UBound($lvItemWidth)]
	Local $lvRect[UBound($lvItemWidth)][4]
	Local $oldText

	$lvHotItem = _GUICtrlListView_GetHotItem($hListView)

	For $i = 0 To UBound($lvItemWidth) - 1
		$temp = _GUICtrlListView_GetSubItemRect($hListView, $lvHotItem, ($i))
		For $o = 0 To 3 ;move to lvRect
			$lvRect[$i][$o] = $temp[$o]
		Next
		$lvItemWidth[$i] = $lvRect[$i][2] - $lvRect[$i][0]
		$lvItemHeight[$i] = $lvRect[$i][3] - $lvRect[$i][1]
	Next

	Select
		Case ($lvRect[4][0] < $mPos) And ($mPos < $lvRect[4][2])
			$lvHotItemSub = 4
			Return
		Case ($lvRect[3][0] < $mPos) And ($mPos < $lvRect[3][2])
			$lvHotItemSub = 3
		Case ($lvRect[2][0] < $mPos) And ($mPos < $lvRect[2][2])
			$lvHotItemSub = 2
		Case ($lvRect[1][0] < $mPos) And ($mPos < $lvRect[1][2])
			$lvHotItemSub = 1
		Case Else
			$lvHotItemSub = -1
			Return
	EndSelect


	;========== Double Click Check ==========
	If ($oldHotItem <> $lvHotItem) Or ($oldHotItemSub <> $lvHotItemSub) Then
		$oldHotItem = $lvHotItem
		$oldHotItemSub = $lvHotItemSub
		Return
	EndIf

	;========== Path Slot double-clicked ========
	If $lvHotItemSub = 2 Then
		$temp = FileSelectFolder("Select the Application Folder", $dir, "", $dir)
		If @error Then Return
		$temp2 = StringSplit(FileGetShortName($temp), "\")
		$arStrDosPaths[$oldHotItem] = $temp2[$temp2[0]] & "\"
		_GUICtrlListView_SetItem($hListView, $arStrDosPaths[$oldHotItem], $oldHotItem, $oldHotItemSub)
		Return
	EndIf

	;========== Exe Slot double-clicked ============
	If $lvHotItemSub = 3 Then
		$temp = FileOpenDialog("Select the Executable to Run", $dir & _GUICtrlListView_GetItemText($hListView, $lvHotItem, 2), "ALL (*.*)")
		If @error Then Return
		$temp = StringSplit($temp, "\")
		_GUICtrlListView_SetItem($hListView, $temp[$temp[0]], $oldHotItem, $oldHotItemSub)
		$arStrExes[$oldHotItem] = $temp[$temp[0]]
;~ 		_ArrayDisplay($arStrExes)
		ReOrgListView()
		Return
	EndIf


	;========== Name Slot double-clicked ===========
	$oldText = _GUICtrlListView_GetItemText($hListView, $lvHotItem, $lvHotItemSub)

	$listItem = _GUICtrlListView_GetItemTextArray($hListView, -1) ; 0 num of cols, 1 txt col 1, 2 txt col 2 ;Retrieves of currently selected item
	$temp = _GUICtrlListView_GetSelectedIndices($hListView)
	GUICtrlSetPos($lvEdit, $lvRect[$lvHotItemSub][0] + 5, $lvRect[$lvHotItemSub][1] + 60, $lvItemWidth[$lvHotItemSub], $lvItemHeight[$lvHotItemSub]) ;add 60 to Y coord to compensate for the position of the listview box
	GUICtrlSetState($lvEdit, $GUI_SHOW)
	GUICtrlSetState($lvEdit, $GUI_FOCUS)
	GUICtrlSetData($lvEdit, $oldText)

	ReadfromListView()
EndFunc   ;==>setItemtoEdit

Func getpath($num)
	$num = StringTrimRight($num, 1) ; just trim to isolate the number... original format has a period and space
	$temp = StringRegExp($strRunBat, "(?:" & $num & " goto )(.*)(?:\H)", 1) ; find the name in the run batch string that follows "## goto "...
	If @error = 1 Then Return "Empty Slot"
	$temp2 = StringRegExp($strRunBat, "(?s)(?i):" & $temp[0] & "(?:\s)(.*?)(?:\s)goto end", 1) ; use the previous name to find the path the exe file is located in
	$temp3 = StringRegExp($temp2[0], "cd (.*?)(?:\s)", 1) ;each line containing cd is another folder level
	$exeName = StringRegExp($temp2[0], "(?:\s)(.*)\z", 1)
	If StringInStr($exeName[0], "Error", 0) Then
		Return $exeName[0]
	EndIf
	Local $path = $dir
	For $i = 0 To (UBound($temp3) - 1)
		$path &= $temp3[$i] & "\"
	Next
	Return $path & $exeName[0]
EndFunc   ;==>getpath

Func editDir()
	Local $temp = FileSelectFolder("Select Directory of Dos Games" & @CRLF & _
			"Do no select the root of a drive i.e. 'C:\'" & @CRLF & _
			"It is recommended to use C:\Games\Dos Games\", "", 1) & "\"
	If Not @error Then
		$dir = $temp
		GUICtrlSetData($GUIBasePath, $dir)
	EndIf
EndFunc   ;==>editDir

Func additem2()
	GUICtrlCreateListViewItem(1 & "|" & "test1" & "|" & "test2" & "|" & "test3" & "|Yes", $hListView)
	sortNumbers()
EndFunc   ;==>additem2

Func addItem()
	Local $insertPos
	Local $itemFound = -1
	Local $fPathAddItem = FileOpenDialog("Select Executable of Game to add", $dir, "All (*.*)")
	If @error Then
		Return
	EndIf
	Local $PathAddItem = StringSplit(StringTrimLeft($fPathAddItem, StringLen($dir)), "\") ;[1] name, [2] exe
	Local $folderAddItem = $PathAddItem[1] & "\"
	Local $nameAddItem = $PathAddItem[1]
	Local $exeAddItem = $PathAddItem[2]
	If @error Then
		Return
	EndIf

	$temp = StringSplit(FileGetShortName($fPathAddItem), "\")
	$temp2 = StringSplit(FileGetShortName($dir), "\")
	Local $folderDosAddItem = $temp[$temp2[0]] & "\"
	Local $arLVNames[_GUICtrlListView_GetItemCount($hListView)]

	For $i = 0 To UBound($arLVNames) - 1
		$arLVNames[$i] = _GUICtrlListView_GetItemText($hListView, $i, 1)
	Next
	$insertPos = UBound($arLVNames)
	For $i = 0 To UBound($arLVNames) - 1
		If StringCompare($arLVNames[$i], $nameAddItem) = 1 Then
			$insertPos = $i
			ExitLoop
		EndIf
	Next

	If FileExists($dir & $folderAddItem & "RunMe.Bat") Then
		GUICtrlCreateListViewItem($insertPos + 1 & "|" & $nameAddItem & "|" & $folderDosAddItem & "|" & $exeAddItem & "|Yes", $hListView)
		GUICtrlSetOnEvent(-1, "setItemtoEdit")
	Else
		GUICtrlCreateListViewItem($insertPos + 1 & "|" & $nameAddItem & "|" & $folderDosAddItem & "|" & $exeAddItem & "|No", $hListView)
		GUICtrlSetOnEvent(-1, "setItemtoEdit")
	EndIf

	ReOrgListView() ; Reorganize the ListView Control

;~ 		Local $B_DESCENDING[_GUICtrlListView_GetColumnCount($hListView)]
;~ 		_GUICtrlListView_SimpleSort($hListView, $B_DESCENDING, 1) ; images are not attached to the rows so I have to resort the images after sorting the text. A Real Pain.Com
;~ 		sortNumbers()

;~ 		For $e = 0 To _GUICtrlListView_GetItemCount($hListView)
;~ 			Select
;~ 				Case (StringLen(_GUICtrlListView_GetItemText($hListView, $e, 3)) > 1) And (StringInStr(_GUICtrlListView_GetItemText($hListView, $e, 3), "Error") = 0) And (_GUICtrlListView_GetItemText($hListView, $e, 4) = "No")
;~ 					_GUICtrlListView_SetItemImage($hListView, $e, 1, 5) ;0 red 1 yellow 2 green
;~ 				Case (StringLen(_GUICtrlListView_GetItemText($hListView, $e, 3)) < 2) Or (StringInStr(_GUICtrlListView_GetItemText($hListView, $e, 3), "Error") = 1)
;~ 					_GUICtrlListView_SetItemImage($hListView, $e, 0, 5) ;0 red 1 yellow 2 green
;~ 				Case (StringLen(_GUICtrlListView_GetItemText($hListView, $e, 3)) > 1) And (StringInStr(_GUICtrlListView_GetItemText($hListView, $e, 3), "Error") = 0) And (_GUICtrlListView_GetItemText($hListView, $e, 4) = "Yes")
;~ 					_GUICtrlListView_SetItemImage($hListView, $e, 2, 5) ;0 red 1 yellow 2 green
;~ 			EndSelect
;~ 		Next

	_GUICtrlListView_EnsureVisible($hListView, $insertPos)
	_GUICtrlListView_SetItemSelected($hListView, $insertPos, "True", "True")
	_GUICtrlListView_SetItemFocused($hListView, $insertPos)
	ReadfromListView()
EndFunc   ;==>addItem

Func delItem()
	If Confirm(1) = True Then
		Local $lvSelItem = _GUICtrlListView_GetSelectedIndices($hListView)
		If $lvSelItem <> "" Then
			_GUICtrlListView_DeleteItem($hListView, $lvSelItem)
		EndIf
	EndIf
	sortNumbers()
EndFunc   ;==>delItem

Func sortNumbers()
	$itemCount = _GUICtrlListView_GetItemCount($hListView)
	For $i = 0 To $itemCount
		_GUICtrlListView_SetItemText($hListView, $i, $i + 1)
	Next
EndFunc   ;==>sortNumbers

Func Confirm($type) ; 1 delete, 2 load, 3 save
	Switch $type
		Case 1 ; delete
			If MsgBox(0x0134, "Confirm Delete", "Are you sure you want to delete this item?" & _
					@CRLF & "Changes will not take permanent effect until saved.") = 6 Then Return 1
			Return 0
		Case 2 ; load
			If MsgBox(0x0134, "Confirm Load", "Are you sure you want to load a file at this time?" & _
					@CRLF & "Loading will erase all changes made to the current batch menu file.") = 6 Then Return 1
			Return 0
		Case 3
;~ 			If MsgBox(0x0144, "Confirm Save", "Are you sure you want to save changes to the current batch menu file?" & _
			Local $temp = MsgBox(0x0143, "Confirm Save", "Do you wish to overwrite any existing Run.Bat's?" & _
					@CRLF & "Select Cancel to cancel save")
			Switch $temp
				Case 2
					Return -1
				Case 6
					Return 1
				Case 7
					Return 0
			EndSwitch

		Case Else
			Return -1
	EndSwitch

EndFunc   ;==>Confirm



Func makeNew()
	$temp = $dir
;~ 	Do
;~ 		$dir = FileSelectFolder("Select Directory of Dos Games" & @CRLF & _
;~ 				"Do no select the root of a drive i.e. 'C:\'" & @CRLF & _
;~ 				"It is recommended to use C:\Games\Dos Games\", "", 1) & "\" ; Select the path of the Games Folder... Returns a path
;~ 		;MsgBox(0,"",StringMid($dir, 2,1))
;~ 	Until (@error = 1) Or ((StringLen($dir) > 4) And ((StringMid($dir, 2, 1) = ":")))
	$dir = "d:\games\dos games\"
	If @error = 1 Then
		MsgBox(0x30, "Operation Aborted", "The operation has been cancelled.")
		$dir = $temp
		Return
	EndIf

	;----------------- $dir already established
	;----------------- $number of menu items and item names can be found


	;----------------- Declare Variables
	Dim $folder = _FileListToArray($dir, "*", 2) ; Returns list of all the folders in the directory
	_ArrayDelete($folder, 0) ; Get rid of the first element, will be confusing later if I don't
	$countMenuItems = UBound($folder)
	;MsgBox(0,"",$countMenuItems)
	DecVar($countMenuItems)
	For $i = 0 To ($countMenuItems - 1)
		$arStrMenuItems[$i] = $folder[$i] ; Give a temporary name for now
		$arStrPaths[$i] = $folder[$i] & "\"; Put the path in $arStrPaths
		$temp = StringSplit(FileGetShortName($dir & $folder[$i]), "\") ; Put the Dos path in $arStrDosPaths
		$arStrDosPaths[$i] = $temp[$temp[0]] & "\"
	Next

	GUICtrlSetData($GUIBasePath, $dir)
	For $i = 0 To $countMenuItems - 1
		If FileExists($dir & $arStrPaths[$i] & "RunMe.Bat") Then
			$temp = FileOpen($dir & $arStrPaths[$i] & "RunMe.Bat", 0)
			$arStrExes[$i] = FileReadLine($temp, 2)
			$temp2 = FileReadLine($temp, 4)
;~ 			MsgBox(0,"",$temp2)
			;\\ quickfix possible error later, reading line 4 instead of 3

			If Not @error Then
				$arStrMenuItems[$i] = StringTrimLeft($temp2, 4)
			EndIf
			FileClose($temp)
			$arStrRunMeBatYN[$i] = "Yes"
		Else
			$arStrRunMeBatYN[$i] = "No"
		EndIf
	Next



	;--------------- Set up the ListViews ----------------
	DisplayMenu()
EndFunc   ;==>makeNew

Func DecVar($count = $countMenuItems) ;Declares variables after finding knowing the number of menu items; also builds menu buttons.
	$countMenus = Round(($countMenuItems / 40) + .49, 0)
	ReDim $hwndMenu[$countMenus] ; These are the handles for the open files that are being read. Note that important to declare here I think. Only temporarily open.
	ReDim $arStrMenus[$countMenus] ; This will contain the batch script for the menu bats
	ReDim $arStrMenuItems[$countMenuItems] ;This will contain the names of the items, will be used in the menu bats
	ReDim $arStrPaths[$countMenuItems] ; This will contain the paths to the executables, will be used in run.bat
	ReDim $arStrDosPaths[$countMenuItems] ; This will contain the Dos version of the paths
	ReDim $arStrExes[$countMenuItems] ; This will contain the exes to be run, will be used in run.bat
	ReDim $arStrLabel[$countMenuItems] ; This will contain the labels to be used in run.bat
	ReDim $arStrRunMeBatYN[$countMenuItems] ; This will contain the string Yes or No depending on the existence of the RunMeBat

	GUICtrlSetData($GUIBasePath, $dir)
EndFunc   ;==>DecVar

Func autoSelectExes() ; Search for RunMeBats. $exesnot yet chosen, or old
	Dim $arError[$countMenuItems][2] ; one for number and one for name
	Dim $countError = 0
	For $i = 0 To ($countMenuItems - 1)
		If FileExists($dir & $arStrPaths[$i] & "RunMe.Bat") = 0 Then
			$arError[$countError][0] = ($i + 1)
			$arError[$countError][1] = $arStrPaths[$i]
			$countError += 1
			$arStrRunMeBatYN[$i] = "No"
		Else
			$temp = FileOpen($dir & $arStrPaths[$i] & "RunMe.Bat", 0)
			$arStrExes[$i] = FileReadLine($temp, 2)
			FileClose($temp)
			$arStrRunMeBatYN[$i] = "Yes"
		EndIf
	Next
	_ArrayDisplay($arStrExes, "Results of Selected Executables")
	If $countError > 0 Then
		_ArrayDisplay($arError, "The Following did not have corresponding RunMe.Bat files in their directories.")
		;_ArrayDisplay($arStrPaths)
	EndIf
EndFunc   ;==>autoSelectExes

;Func makeRunMeBats($count = $countMenuItems, $paths = $arStrPaths, $exes = $arStrExes) ; Make RunMeBats. Count of items, paths array, exes array.
Func makeRunMeBats() ; Make RunMeBats. Count of items, paths array, exes array.

	;GUISwitch($hGUI)
	ReadfromListView() ; ensure arrays are all update and concurrent with the listview control
	$tempwindow = GUICreate("Progress", 200, 80, @DesktopWidth / 2, @DesktopHeight / 2)
	GUISwitch($tempwindow)
	Local $hwndProgress = GUICtrlCreateProgress(25, 10, 150, 20)
	Local $hwndProgressLabel = GUICtrlCreateLabel("", 25, 45, 150, 30)
	GUISetState(@SW_SHOW, $tempwindow)
	GUICtrlSetData(-1, 0)
	;MsgBox(0,"","made it inside")
	Dim $hwndFile[$countMenuItems]
	;_ArrayDisplay($arStrExes)
	;_ArrayDisplay($arStrPaths)
	;MsgBox(0,"",$dir & $arStrPaths[0] & "RunMe.Bat")
	For $i = 0 To ($countMenuItems - 1)
		GUICtrlSetData($hwndProgress, (($i + 1) / $countMenuItems) * 100)
		GUICtrlSetData($hwndProgressLabel, _GUICtrlListView_GetItemText($hListView, $i, 1))
		Sleep(15)
		;If (FileExists($dir & $arStrPaths[$i] & "RunMe.Bat") = 0) And (StringLen($arStrExes[$i]) > 1) AND (StringInStr($arStrExes[$i],"Error") = 0) AND (StringInStr($arStrExes[$i],"RunMe.bat") = 0) Then
;~ 		If (StringLen($arStrExes[$i]) > 1) And (StringInStr($arStrExes[$i], "Error") = 0) And (StringInStr($arStrExes[$i], "RunMe.bat") = 0) Then
;~ 		MsgBox(262144, 'Debug line ~' & @ScriptLineNumber, 'Selection:' & @LF & '$arStrRunMeBatYN[$i]' & @LF & @LF & 'Return:' & @LF & $arStrRunMeBatYN[$i]) ;### Debug MSGBOX

;~ 		If (StringLen($arStrExes[$i]) > 1) And (StringInStr($arStrExes[$i], "Error") = 0) And ($arStrRunMeBatYN[$i] = "No") Then
		If (StringLen($arStrExes[$i]) > 1) And (StringInStr($arStrExes[$i], "Error") = 0) Then
;~ 			MsgBox(0, "", "masuk")
			; I replaced the failsafe for the file exist RunMe.bat check with checking the string of arStrExes to see if contains RunMe.bat.
			; This is better because if the user has a want to REPLACE the contents of RunMe.Bat, he/she is able to do so.

			;If (FileExists($dir & $arStrPaths[$i] & "RunMe.Bat") = 1) Then MsgBox(0,"","1")
			;If (StringLen($arStrExes[$i]) > 1) Then MsgBox(0,"","2")
			;If (StringInStr($arStrExes[$i],"Error") = 0) Then MsgBox(0,"","3")
			;Exit
			;ExitLoop
			;------------- Error -------------- Do not write if $arStrExes[$i] string length < 1 or if there is an occurance of "Error" in the string
			;------------- Error -------------- if file exists, skip writing because it is likely that the exe selected is already the same as RunMe.Bat
			;---------------------------------- writing a new file of RunMe.Bat will create one that calls itself.
			;MsgBox(0,"","made past failsafes")

			$hwndFile[$i] = FileOpen($dir & $arStrPaths[$i] & "RunMe.Bat", 2) ; create if does not exist
			FileWrite($hwndFile[$i], "" & _
					"@echo off" & @CRLF & _
					$arStrExes[$i] & @CRLF & _
					"REM " & $arStrMenuItems[$i])
		EndIf
	Next
	For $i = 0 To ($countMenuItems - 1)
		FileClose($hwndFile[$i])
	Next
	GUIDelete($tempwindow)
	GUISwitch($hGUI)
	ReOrgListView()
	MsgBox(0x40, "Finished", "Operation was completed successfully")
EndFunc   ;==>makeRunMeBats

; This will Get the user to select the exe's for each folder that was previously located in the games directory
; Then each the exepaths and folders will be converted to the Dos naming system and made into a script usable in a batch file AFTER being called
; Note this does not include the initial part of the batch file

Func saveBatch()
	Local $temp
	Switch Confirm(3)
		Case -1 ; Cancel
			Return
		Case 0 ; No
			MsgBox(0, "Saving Run.Bat", "Saving Run.Bat in Base Directory" & @CRLF & $dir & @CRLF & @CRLF & @CRLF & "Moving Old Files to zBackUp Folder")
			$temp = 0

		Case 1 ; Yes
			$temp = 1
			MsgBox(0, "Saving Run.Bat", "Saving Run.Bat in Base Directory" & @CRLF & $dir & @CRLF & @CRLF & @CRLF & "Overwriting old batch menus that may exist.")
	EndSwitch
	Encode()
	If $temp = 0 Then
		If FileExists($dir & "Run.Bat") Then
			$temp = UBound(StringRegExp($strRunBat, "==", 3)) ;this effectively tells how many items are in the menus
			$tempCountMenus = Round(($temp / 40) + .49, 0)
			;now I know how many menus there are and that there is a run.bat... it's time to rename them, Or move them to a folder named by the date&time
			$name = @YEAR & "-" & @MON & "-" & @MDAY & "    " & @HOUR & "." & @MIN & "." & @SEC
			FileMove($dir & "Run.Bat", $dir & "zBackUp\" & $name & "\", 8)
			For $i = 0 To $tempCountMenus - 1
				FileMove($dir & ($i + 1) & ".Bat", $dir & "zBackUp\" & $name & "\", 8)
			Next
		EndIf
	EndIf

	Local $tempFOpen = FileOpen($dir & "Run.Bat", 10)
	FileWrite($tempFOpen, $strRunBat)
	FileClose($tempFOpen)
	For $i = 0 To $countMenus - 1
		$tempFOpen = FileOpen($dir & ($i + 1) & ".Bat", 10)
		FileWrite($tempFOpen, $arStrMenus[$i])
		FileClose($tempFOpen)
	Next

EndFunc   ;==>saveBatch

Func Encode()
	ReadfromListView()
	EncodeMenus()
	EncodeRunBat()
EndFunc   ;==>Encode


Func ReadfromListView()
	$countMenuItems = _GUICtrlListView_GetItemCount($hListView)
	$countMenus = Round(($countMenuItems / 40) + .49, 0)
	Dim $arStrMenus[$countMenus] ; This will contain the batch script for the menu bats
	Dim $arStrMenuItems[$countMenuItems] ;This will contain the names of the items, will be used in the menu bats
	Dim $arStrPaths[$countMenuItems] ; This will contain the paths to the executables, will be used in run.bat
	Dim $arStrDosPaths[$countMenuItems] ; This will contain the Dos version of the paths
	Dim $arStrExes[$countMenuItems] ; This will contain the exes to be run, will be used in run.bat
	Dim $arStrLabel[$countMenuItems] ; This will contain the labels to be used in run.bat
	Dim $arStrRunMeBatYN[$countMenuItems] ; This will contain the string Yes or No depending on the existence of the RunMeBat

	Local $temp
	Local $temp2
	For $i = 0 To $countMenuItems - 1
		$temp = _GUICtrlListView_GetItemTextArray($hListView, $i)
		$arStrMenuItems[$i] = $temp[2]
		$arStrDosPaths[$i] = $temp[3]
		$arStrLabel[$i] = StringTrimRight($temp[3], 1)
;~ 	    $arStrPaths[$i] = StringRegExp(FileGetLongName($dir&$temp[3]),"[^\\]*(?:\\)\z",1)
		$temp2 = FileGetLongName($dir & $arStrDosPaths[$i])
		$arStrPaths[$i] = StringTrimLeft($temp2, StringLen($dir)) & "\"
		$arStrExes[$i] = $temp[4]
		$arStrRunMeBatYN[$i] = $temp[5]
	Next


EndFunc   ;==>ReadfromListView

Func EncodeMenus()
	$ScriptHeader = "" & _	;			Header for the menus
			"@echo off" & @CRLF & _
			"cls" & @CRLF & _
			"echo                                   Menu XX" & @CRLF & _ ;I'm using the xx to use with a stringreplace later :)
			"echo." & @CRLF
	$Bodytemplate = "echo xx.                                        " ; this is the template line for the menu. I use this because It has aprox 40 spaces.

	Dim $arStrMenus[$countMenus] ; clear the previous contents of $arStrMenus
	Local $scriptMenus[$countMenus] ; Make a local array to later pass to global array
	For $e = 0 To ($countMenus - 1) ; Let's put it in a big loop

		Dim $scriptBody[40] ; Make it into an array to make it easier
		$scriptMenus[$e] &= $ScriptHeader ; Attach the Header First
		$scriptMenus[$e] = StringReplace($scriptMenus[$e], "XX", ($e + 1))
		For $i = 1 To 40 ; This will make 1 menu
			$scriptBody[$i - 1] = StringReplace($Bodytemplate, "xx", (($e * 40) + $i))
			Select
				Case (($e * 40) + $i) > UBound($arStrMenuItems) ; check to see if it will be out of range
					$scriptBody[$i - 1] = StringTrimLeft($scriptBody[$i - 1], 5)
				Case (($e * 40) + $i) < 10 ; special case single digit
					$scriptBody[$i - 1] = StringReplace($scriptBody[$i - 1], 9, $arStrMenuItems[$i + ($e * 40) - 1])
				Case ((9 < (($e * 40) + $i)) And ((($e * 40) + $i) < 21)) Or (($e * 40) < (($e * 40) + $i) And (($e * 40) + $i) < (21 + $e * 40))
					$scriptBody[$i - 1] = StringReplace(StringTrimRight($scriptBody[$i - 1], 1), 10, $arStrMenuItems[$i + ($e * 40) - 1])
				Case (($e * 40) + $i) > (20 + $e * 40)
					$scriptBody[$i - 1] = StringReplace(StringTrimLeft($scriptBody[$i - 1], 5), 5, $arStrMenuItems[$i + ($e * 40) - 1])
			EndSelect
		Next
		For $i = 1 To 20
			$scriptMenus[$e] &= $scriptBody[$i - 1] & $scriptBody[$i + 19] & @CRLF
		Next

		$arStrMenus[$e] = $scriptMenus[$e]
	Next
EndFunc   ;==>EncodeMenus

Func EncodeRunBat()
	MsgBox(0, "ubound", UBound($arStrRunMeBatYN))
	Local $scriptRun = "@echo off" & @CRLF & @CRLF
	For $i = 0 To ($countMenuItems - 1)
		$scriptRun &= "If %1 == " & ($i + 1) & " goto " & $arStrLabel[$i] & @CRLF
	Next
	$scriptRun &= @CRLF
	Local $temp
	For $i = 0 To ($countMenuItems - 1) ; start putting it together to make it work
		If $arStrPaths[$i] <> "Error" Then
			$scriptRun &= ":" & $arStrLabel[$i] & @CRLF & _	   ; function name
					"cd " & $arStrDosPaths[$i] & @CRLF ; go to appropriate folder
			Select
				Case ($arStrRunMeBatYN[$i] = "Yes")
					$scriptRun &= "RunMe.bat" & @CRLF ; run the file
				Case ($arStrRunMeBatYN[$i] = "No")
					$scriptRun &= $arStrExes[$i] & @CRLF ; run the file
			EndSelect
			$scriptRun &= "goto end" & @CRLF & @CRLF

		EndIf
;~ 		If $arStrPaths[$i] <> "Error" Then
;~ 			$scriptRun &= ":" & $arStrLabel[$i] & @CRLF & _	   ; function name
;~ 					"cd " & $arStrDosPaths[$i] & @CRLF & _	; go to appropriate folder
;~ 					$arStrExes[$i] & @CRLF & _ ; run the file
;~ 					"goto end" & @CRLF & _	; go to end
;~ 					@CRLF
;~ 		EndIf
	Next
	$scriptRun &= ":Error" & @CRLF & _
			"echo Error. No executable selected or available." & @CRLF & _
			"goto end" & @CRLF & @CRLF & _
			":end" & @CRLF & "cd G:\"
	$strRunBat = $scriptRun
EndFunc   ;==>EncodeRunBat

Func Decode($str = $strRunBat, $aR = $arStrMenus) ; needs: strRunBat, arStrMenus......... $dir NEEDS to be choosen first
	DecodeArStrMenus()
	DecodeStrRunBat()
EndFunc   ;==>Decode

Func DecodeArStrMenus($aR = $arStrMenus) ; pass the arStrMenus here to convert to arMenuItems
	For $i = 0 To (UBound($aR) - 1)
		Local $itemArray = StringRegExp($aR[$i], "(?:[0-9]. )(.*?)(?:  )", 3) ; makes a list of all the items in the menu
		For $o = 0 To 19
			If ($o + ($i * 40)) < $countMenuItems Then $arStrMenuItems[$o + ($i * 40)] = $itemArray[$o * 2]
			If ($o + 20 + ($i * 40)) < $countMenuItems Then $arStrMenuItems[$o + 20 + ($i * 40)] = $itemArray[$o * 2 + 1]
		Next
	Next
	;_ArrayDisplay($arStrMenuItems)
EndFunc   ;==>DecodeArStrMenus

Func DecodeStrRunBat($str = $strRunBat) ; needs the strRunBat

	For $i = 0 To ($countMenuItems - 1)
		;temp[0] will be the name of the label
		;temp2[0] will be the chunk of data between the label and goto end
		;temp3[0] will be the folder levels *may be more than one
		;exeName[0] will be the name of the exe

		$temp = StringRegExp($str, "(?: " & ($i + 1) & " goto )(.*)(?:\H)", 1) ; find the name in the run batch string that follows "## goto "...
		If @error = 1 Then
			MsgBox(0, "Error", "There has been an error around line 815 of the program, please contact your nearest Chris for assistance.")
			Return
		EndIf
		$arStrLabel[$i] = $temp[0]
		$temp2 = StringRegExp($strRunBat, "(?s)(?i):" & $temp[0] & "(?:\s)(.*?)(?:\s)goto end", 1) ; use the previous name to find the path the exe file is located in
		If @error = 1 Then
			MsgBox(0, "Error", "There has been an error around line 821 of the program, please contact your nearest Chris for assistance.")
			Return
		EndIf
		$temp3 = StringRegExp($temp2[0], "cd (.*?)(?:\s)", 1) ;each line containing cd is another folder level
		$exeName = StringRegExp($temp2[0], "(?:\s)(.*)\z", 1)
		If StringInStr($exeName[0], "Error", 0) Then
			$arStrDosPaths[$i] = "Error"
			$arStrPaths[$i] = "Error"
			$arStrExes[$i] = $exeName[0]
			$arStrRunMeBatYN[$i] = "No"
		Else
			Local $path = ""
			For $o = 0 To (UBound($temp3) - 1)
				$path &= $temp3[$o]
			Next
			$arStrDosPaths[$i] = $path
			$temp = FileGetLongName($dir & $arStrDosPaths[$i])
			$arStrPaths[$i] = StringTrimLeft($temp, StringLen($dir)) & "\"
			$arStrExes[$i] = $exeName[0]

			If FileExists($dir & $arStrPaths[$i] & "RunMe.Bat") Then
				$temp = FileOpen($dir & $arStrPaths[$i] & "RunMe.Bat", 0)
				$arStrExes[$i] = FileReadLine($temp, 2)
;~ 				$arStrMenuItems[$i] = FileReadLine($temp, 3)
;~ 				Local $temp2 = FileReadLine($temp)
;~ 				$arStrExes[$i] = _StringBetween($temp2,"/s","/sRem")
				ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $arStrExes[$i] = ' & $arStrExes[$i] & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console
;~ 				ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $arStrExes[$i] = ' & $arStrExes[$i] & @crlf & '>Error code: ' & @error & @crlf) ;### Debug Console
				FileClose($temp)
				$arStrRunMeBatYN[$i] = "Yes"
			Else
				$arStrRunMeBatYN[$i] = "No"
			EndIf

		EndIf

	Next
EndFunc   ;==>DecodeStrRunBat

Func ReadRunBat($path) ; needs path  aka $dir
	Global $hwndRunBat = FileOpen($path & "Run.bat", 0)
;~ 	MsgBox(262144,'Debug line ~' & @ScriptLineNumber,'Selection:' & @lf & '$path' & @lf & @lf & 'Return:' & @lf & $path) ;### Debug MSGBOX
	If $hwndRunBat = -1 Then
		MsgBox(0x30, "Error", "An error has occured while trying to open Run.bat" & @CRLF & $path)
		Return
	EndIf
	GUICtrlSetData($GUIBasePath, $path)
	Global $strRunBat = FileRead($hwndRunBat)
	$countMenuItems = UBound(StringRegExp($strRunBat, "==", 3)) ;this effectively tells how many items are in the menus
	DecVar($countMenuItems)

	For $i = 1 To $countMenus
		$hwndMenu[$i - 1] = FileOpen($path & $i & ".bat", 0)
		If $hwndMenu[$i - 1] = -1 Then
			MsgBox(64, "Error", "The Menu Batch Files were not all found, Please make a new batch file menu by clicking make new batch file on the parent window.")
			Return
		EndIf
		$arStrMenus[$i - 1] = FileRead($hwndMenu[$i - 1])
	Next

	;close all opened files
	For $i = 1 To UBound($hwndMenu)
		FileClose($hwndMenu[$i - 1])
	Next

	FileClose($hwndRunBat)
EndFunc   ;==>ReadRunBat

Func ReOrgListView()
	Local $B_DESCENDING[_GUICtrlListView_GetColumnCount($hListView)]
	_GUICtrlListView_SimpleSort($hListView, $B_DESCENDING, 1) ; images are not attached to the rows so I have to resort the images after sorting the text. A Real Pain.Com
	sortNumbers()

	For $e = 0 To _GUICtrlListView_GetItemCount($hListView)
		Select
			Case (StringLen(_GUICtrlListView_GetItemText($hListView, $e, 3)) > 1) And (StringInStr(_GUICtrlListView_GetItemText($hListView, $e, 3), "Error") = 0) And (_GUICtrlListView_GetItemText($hListView, $e, 4) = "No")
				_GUICtrlListView_SetItemImage($hListView, $e, 1, 5) ;0 red 1 yellow 2 green
			Case (StringLen(_GUICtrlListView_GetItemText($hListView, $e, 3)) < 2) Or (StringInStr(_GUICtrlListView_GetItemText($hListView, $e, 3), "Error") = 1)
				_GUICtrlListView_SetItemImage($hListView, $e, 0, 5) ;0 red 1 yellow 2 green
			Case (StringLen(_GUICtrlListView_GetItemText($hListView, $e, 3)) > 1) And (StringInStr(_GUICtrlListView_GetItemText($hListView, $e, 3), "Error") = 0) And (_GUICtrlListView_GetItemText($hListView, $e, 4) = "Yes")
				_GUICtrlListView_SetItemImage($hListView, $e, 2, 5) ;0 red 1 yellow 2 green
		EndSelect
	Next
EndFunc   ;==>ReOrgListView
